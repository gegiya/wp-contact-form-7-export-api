# README #

This plugin extends Contact Form 7 REST API with an endpoint to export stored contact forms as CSV file. 
It supports the basic authentication with username and password.

### Pre-requisites ###

Following plugings must be installed before installing this plugin:

- [JSON Basic Authentication](https://github.com/WP-API/Basic-Auth)
- Contact Form 7
- Contact Form CFDB7

### Installation ###
- Download the plugin as zip archive
- Extract the archive into wordpress plugins directory

### Setting up ###
- in order to enable basic authentication for the wp_json path, append following lines to the `.htaccess` file in the root directory of your Wordpress installation
***
```
SetEnvIf Authorization .+ HTTP_AUTHORIZATION=$0
RewriteBase /
RewriteRule ^wp-json/.* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization},L]
```
***
- activate plugin in the admin console
- enable `edit_posts` capability for the user which credential will be used to call export endpoint  

### Usage ###

#### cURL ####
***
```
curl --user username:password https://example.com/wp-json/contact-form-7/v1/contact-forms/<form_id>/export
```
***
### Contacts ###

gegiya@gmail.com