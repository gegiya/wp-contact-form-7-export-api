<?php
/**
 * Plugin Name: Contact Form 7 export API
 * Plugin URI: https://bitbucket.org/gegiya/wp-contact-form-7-export-api/src/master/
 * Description: Plugin for Wordpress that provides an API to export contact forms as CSV file
 * Version: 1.0
 * Author: Georgiy Gegiya
 * Author URI: https://bitbucket.org/gegiya/
 */ 

add_action( 'rest_api_init', 'wpcf7_export_api_init', 10, 0 );

function wpcf7_export_api_init() {
	$namespace = 'contact-form-7/v1';

	register_rest_route( $namespace,
		'/contact-forms/(?P<id>\d+)/export',
		array(
			array(
				'methods' => WP_REST_Server::READABLE,
				'callback' => 'wpcf7_rest_export_contact_forms',
			)
		)
	);

}

function wpcf7_rest_export_contact_forms( WP_REST_Request $request ) {
	$id = (int) $request->get_param( 'id' );
	$item = wpcf7_contact_form( $id );

	if ( ! $item ) {
		return new WP_Error( 'wpcf7_not_found',
			__( "The requested contact form was not found.", 'contact-form-7' ),
			array( 'status' => 404 ) );
	}
	
	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];

	wp_authenticate( $username, $password );
	
	if ( ! current_user_can( 'wpcf7_read_contact_forms', $id ) ) {
		return new WP_Error( 'wpcf7_forbidden',
			__( "You are not allowed to access the requested contact form.", 'contact-form-7' ),
			array( 'status' => 403 ) );
	}
	
	require_once 'include/export-csv.php';

	$csv = new Export_CSV();
       	$csv->export_to_csv($id);
	
	
}